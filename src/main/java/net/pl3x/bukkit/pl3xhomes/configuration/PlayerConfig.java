package net.pl3x.bukkit.pl3xhomes.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.pl3x.bukkit.pl3xhomes.Main;

public class PlayerConfig extends YamlConfiguration {
	private static Map<UUID, PlayerConfig> configs = new HashMap<UUID, PlayerConfig>();

	public static PlayerConfig getConfig(Player player) {
		return getConfig(player.getUniqueId());
	}

	public static PlayerConfig getConfig(OfflinePlayer player) {
		UUID uuid = player.getUniqueId();
		if (!new File(Main.getPlugin(Main.class).getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml").exists()) {
			return null;
		}
		return getConfig(uuid);
	}

	public static PlayerConfig getConfig(UUID uuid) {
		synchronized (configs) {
			if (configs.containsKey(uuid)) {
				return configs.get(uuid);
			}
			PlayerConfig config = new PlayerConfig(uuid);
			configs.put(uuid, config);
			return config;
		}
	}

	public static void removeConfigs() {
		Collection<PlayerConfig> oldConfs = new ArrayList<PlayerConfig>(configs.values());
		synchronized (configs) {
			for (PlayerConfig config : oldConfs) {
				config.discard();
			}
		}
	}

	private File file = null;
	private Object saveLock = new Object();
	private UUID uuid;

	public PlayerConfig(UUID uuid) {
		super();
		file = new File(Main.getPlugin(Main.class).getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml");
		this.uuid = uuid;
		reload();
	}

	@SuppressWarnings("unused")
	private PlayerConfig() {
		uuid = null;
	}

	private void reload() {
		synchronized (saveLock) {
			try {
				load(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void save() {
		synchronized (saveLock) {
			try {
				save(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void discard() {
		discard(false);
	}

	public void discard(boolean save) {
		if (save) {
			save();
		}
		synchronized (configs) {
			configs.remove(uuid);
		}
	}

	public Location getLocation(String path) {
		if (get(path) == null) {
			return null;
		}
		World world = Bukkit.getWorld(getString(path + ".world", ""));
		if (world == null) {
			return null;
		}
		double x = getDouble(path + ".x");
		double y = getDouble(path + ".y");
		double z = getDouble(path + ".z");
		float pitch = (float) getDouble(path + ".pitch");
		float yaw = (float) getDouble(path + ".yaw");
		return new Location(world, x, y, z, yaw, pitch);
	}

	public void setLocation(String path, Location location) {
		if (location == null) {
			set(path, null);
			return;
		}
		set(path + ".world", location.getWorld().getName());
		set(path + ".x", location.getX());
		set(path + ".y", location.getY());
		set(path + ".z", location.getZ());
		set(path + ".pitch", location.getPitch());
		set(path + ".yaw", location.getYaw());
	}

	public int getHomesCount() {
		List<Map<?, ?>> map = getMapList("home");
		if (map == null) {
			return 0;
		}
		return map.size();
	}

	public List<String> getMatchingHomeNames(String name) {
		name = name.toLowerCase();
		List<String> list = new ArrayList<String>();
		ConfigurationSection cfg = getConfigurationSection("home");
		if (cfg == null) {
			return null;
		}
		for (String home : cfg.getValues(false).keySet()) {
			if (home.toLowerCase().startsWith(name)) {
				list.add(home);
			}
		}
		return list;
	}
}
