package net.pl3x.bukkit.pl3xhomes.configuration;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.pl3x.bukkit.pl3xhomes.Logger;
import net.pl3x.bukkit.pl3xhomes.Main;

public enum Lang {
	ERROR_COMMAND_NO_PERMISSION("&4You do not have permission for that command!"),
	ERROR_PLAYER_COMMAND("&4This command is only available to players."),
	ERROR_PLAYER_NOT_FOUND("&4That player does not exist!"),

	HOME_DOES_NOT_EXIST("&4That home does not exist!"),

	HOME_DELETED("&dThe home &7{home} &dhas been deleted."),

	HOME_LIST("&dHomes: {home-list}"),
	HOME_LIST_ENTRY("&7{home}&e, "),
	HOME_LIST_EMPTY("&4No homes found!"),

	HOME("&dGoing to home &7{home}&d."),

	HOME_SET("&dHome &7{home} &dset."),
	HOME_SET_MAX("&4Max number of homes reached! &e(&7{limit}&e)"),

	HOME_EXEMPT("&4You cannot go to that player's home!"),
	HOME_DELETE_EXEMPT("&4You may not delete that player's home!"),
	HOME_LIST_EXEMPT("&4You may not list that player's homes!"),
	HOME_SET_EXEMPT("&4Cannot set that player's home!");

	private String def;

	private File configFile;
	private FileConfiguration config;

	private Lang(String def) {
		this.def = def;
		configFile = new File(Main.getPlugin(Main.class).getDataFolder(), Config.LANGUAGE_FILE.getString());
		saveDefault();
		reload();
	}

	public void reload() {
		config = YamlConfiguration.loadConfiguration(configFile);
	}

	public void saveDefault() {
		if (!configFile.exists()) {
			Main.getPlugin(Main.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
		}
	}

	@Override
	public String toString() {
		String key = name().toLowerCase().replace("_", "-");
		String value = config.getString(key, def);
		if (value == null) {
			Logger.error("Missing lang data: " + key);
			value = "&c[missing lang data]";
		}
		return ChatColor.translateAlternateColorCodes('&', value);
	}

	public String replace(String find, String replace) {
		return toString().replace(find, replace);
	}
}
