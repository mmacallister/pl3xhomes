package net.pl3x.bukkit.pl3xhomes;

import java.io.IOException;

import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import net.pl3x.bukkit.pl3xhomes.commands.DelHome;
import net.pl3x.bukkit.pl3xhomes.commands.Home;
import net.pl3x.bukkit.pl3xhomes.commands.Homes;
import net.pl3x.bukkit.pl3xhomes.commands.SetHome;
import net.pl3x.bukkit.pl3xhomes.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xhomes.listener.PlayerListener;

public class Main extends JavaPlugin {
	@Override
	public void onEnable() {
		saveDefaultConfig();

		getServer().getPluginManager().registerEvents(new PlayerListener(), this);

		getCommand("delhome").setExecutor(new DelHome());
		getCommand("home").setExecutor(new Home());
		getCommand("homes").setExecutor(new Homes());
		getCommand("sethome").setExecutor(new SetHome());

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			Logger.warn("&4Failed to start Metrics: &e" + e.getMessage());
		}

		Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
	}

	@Override
	public void onDisable() {
		PlayerConfig.removeConfigs();

		Logger.info(getName() + " disabled!");
	}
}
