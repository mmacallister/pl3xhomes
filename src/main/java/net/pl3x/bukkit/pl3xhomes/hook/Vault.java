package net.pl3x.bukkit.pl3xhomes.hook;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.permission.Permission;

public class Vault {
	private static Permission permission = null;

	public static boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}

	public static Permission getPermission() {
		if (permission == null) {
			setupPermissions();
		}
		return permission;
	}
}
