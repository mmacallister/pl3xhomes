package net.pl3x.bukkit.pl3xhomes.manager;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.pl3x.bukkit.pl3xhomes.configuration.Lang;

public class ChatManager {
	public static void sendMessage(CommandSender sender, Lang message) {
		sendMessage(sender, message.toString());
	}

	public static void sendMessage(CommandSender sender, String message) {
		if (message == null || ChatColor.stripColor(message).equals("")) {
			return;
		}
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}
}
