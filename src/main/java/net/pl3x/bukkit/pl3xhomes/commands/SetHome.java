package net.pl3x.bukkit.pl3xhomes.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import net.pl3x.bukkit.pl3xhomes.configuration.Lang;
import net.pl3x.bukkit.pl3xhomes.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xhomes.manager.ChatManager;
import net.pl3x.bukkit.pl3xhomes.manager.PermManager;

public class SetHome implements TabExecutor {

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return null;
		}
		if (args.length == 1) {
			return PlayerConfig.getConfig((Player) sender).getMatchingHomeNames(args[0]);
		}
		if (args.length == 2) {
			String name = args[1].trim().toLowerCase();
			List<String> list = new ArrayList<String>();
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.getName().toLowerCase().startsWith(name)) {
					list.add(player.getName());
				}
			}
			return list;
		}
		return null;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			ChatManager.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND);
			return true;
		}

		if (!PermManager.hasPerm(sender, "command.sethome")) {
			ChatManager.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
			return true;
		}

		Player player = (Player) sender;
		PlayerConfig config = PlayerConfig.getConfig(player);
		String home = (args.length > 0) ? args[0] : "home";
		int limit = getHomesLimit(player);

		if (args.length > 1) {
			if (!PermManager.hasPerm(sender, "command.sethome.other")) {
				ChatManager.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
				return true;
			}

			OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
			if (target == null) {
				ChatManager.sendMessage(sender, Lang.ERROR_PLAYER_NOT_FOUND);
				return true;
			}

			if (PermManager.hasPerm(target, "command.sethome.exempt")) {
				ChatManager.sendMessage(sender, Lang.HOME_SET_EXEMPT);
				return true;
			}

			config = PlayerConfig.getConfig(target);
			limit = getHomesLimit(target);
		}

		if (limit >= 0 && config.getHomesCount() >= limit) {
			ChatManager.sendMessage(sender, Lang.HOME_SET_MAX.replace("{limit}", Integer.toString(limit)));
			return true;
		}

		config.setLocation("home." + home, player.getLocation());
		config.save();
		ChatManager.sendMessage(sender, Lang.HOME_SET.replace("{home}", home));
		return true;
	}

	private int getHomesLimit(OfflinePlayer target) {
		int limit = 0;
		int maxlimit = 1024;
		if (PermManager.hasPerm(target, "command.sethome.limit.*")) {
			return -1;
		}
		for (int i = 0; i < maxlimit; i++) {
			if (PermManager.hasPerm(target, "command.sethome.limit." + i) && i > limit) {
				limit = i;
			}
		}
		return limit;
	}

}
