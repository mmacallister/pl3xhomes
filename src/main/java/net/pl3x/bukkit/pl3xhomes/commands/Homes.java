package net.pl3x.bukkit.pl3xhomes.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import net.pl3x.bukkit.pl3xhomes.configuration.Lang;
import net.pl3x.bukkit.pl3xhomes.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xhomes.manager.ChatManager;
import net.pl3x.bukkit.pl3xhomes.manager.PermManager;

public class Homes implements TabExecutor {

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			String name = args[0].trim().toLowerCase();
			List<String> list = new ArrayList<String>();
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.getName().toLowerCase().startsWith(name)) {
					list.add(player.getName());
				}
			}
			return list;
		}
		return null;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			ChatManager.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND);
			return true;
		}

		if (!PermManager.hasPerm(sender, "command.homes")) {
			ChatManager.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
			return true;
		}

		PlayerConfig config = PlayerConfig.getConfig((Player) sender);

		if (args.length > 0) {
			if (!PermManager.hasPerm(sender, "command.homes.other")) {
				ChatManager.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
				return true;
			}

			OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
			if (target == null) {
				ChatManager.sendMessage(sender, Lang.ERROR_PLAYER_NOT_FOUND);
				return true;
			}

			if (PermManager.hasPerm(target, "command.homes.exempt")) {
				ChatManager.sendMessage(sender, Lang.HOME_LIST_EXEMPT);
				return true;
			}
			
			config = PlayerConfig.getConfig(target);
		}

		ConfigurationSection cfgs = config.getConfigurationSection("home");
		if (cfgs == null) {
			ChatManager.sendMessage(sender, Lang.HOME_LIST_EMPTY);
			return true;
		}

		Map<String, Object> opts = cfgs.getValues(false);
		if (opts.keySet().isEmpty()) {
			ChatManager.sendMessage(sender, Lang.HOME_LIST_EMPTY);
			return true;
		}

		StringBuilder sb = new StringBuilder();
		for (String home : opts.keySet()) {
			if (sb != null) {
				sb.append(Lang.HOME_LIST_ENTRY.replace("{home}", home));
			}
		}

		String homes = sb.toString();
		homes = homes.substring(0, homes.length() - 4);

		ChatManager.sendMessage(sender, Lang.HOME_LIST.replace("{home-list}", homes));
		return true;
	}

}
