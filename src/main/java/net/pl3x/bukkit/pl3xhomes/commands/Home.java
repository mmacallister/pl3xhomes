package net.pl3x.bukkit.pl3xhomes.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import net.pl3x.bukkit.pl3xhomes.configuration.Lang;
import net.pl3x.bukkit.pl3xhomes.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xhomes.manager.ChatManager;
import net.pl3x.bukkit.pl3xhomes.manager.PermManager;

public class Home implements TabExecutor {

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return null;
		}
		if (args.length == 1) {
			return PlayerConfig.getConfig((Player) sender).getMatchingHomeNames(args[0]);
		}
		if (args.length == 2) {
			String name = args[1].trim().toLowerCase();
			List<String> list = new ArrayList<String>();
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.getName().toLowerCase().startsWith(name)) {
					list.add(player.getName());
				}
			}
			return list;
		}
		return null;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			ChatManager.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND);
			return true;
		}

		if (!PermManager.hasPerm(sender, "command.home")) {
			ChatManager.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
			return true;
		}

		Player player = (Player) sender;
		PlayerConfig config = PlayerConfig.getConfig(player);
		String home = (args.length > 0) ? args[0] : "home";

		if (args.length > 1) {
			if (!PermManager.hasPerm(sender, "command.home.other")) {
				ChatManager.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION);
				return true;
			}

			OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
			if (target == null) {
				ChatManager.sendMessage(sender, Lang.ERROR_PLAYER_NOT_FOUND);
				return true;
			}

			if (PermManager.hasPerm(target, "command.home.exempt")) {
				ChatManager.sendMessage(sender, Lang.HOME_EXEMPT);
				return true;
			}
			
			config = PlayerConfig.getConfig(target);
		}

		Location loc = config.getLocation("home." + home);
		if (loc == null) {
			ChatManager.sendMessage(sender, Lang.HOME_DOES_NOT_EXIST);
			return true;
		}

		ChatManager.sendMessage(sender, Lang.HOME.replace("{home}", home));
		player.teleport(loc);
		return true;
	}

}
