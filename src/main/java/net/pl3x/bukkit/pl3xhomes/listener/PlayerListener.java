package net.pl3x.bukkit.pl3xhomes.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import net.pl3x.bukkit.pl3xhomes.configuration.PlayerConfig;

public class PlayerListener implements Listener {
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		PlayerConfig.getConfig(event.getPlayer()).discard();
	}
}
